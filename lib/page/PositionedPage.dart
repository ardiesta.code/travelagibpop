import 'package:flutter/material.dart';

class PositionedPage extends StatefulWidget {
  PositionedPage({Key key}) : super(key: key);

  @override
  _PositionedPageState createState() => _PositionedPageState();
}

class _PositionedPageState extends State<PositionedPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Positioned"),
        ),
        );
  }
}