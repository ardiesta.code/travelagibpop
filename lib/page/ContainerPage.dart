import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

class ContainerPage extends StatefulWidget {
  ContainerPage({Key key}) : super(key: key);

  @override
  _ContainerPageState createState() => _ContainerPageState();
}

class _ContainerPageState extends State<ContainerPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Container"),),
      body: Center(
        child: Column(children: <Widget>[
          _container(),
          Container(
            color: Colors.yellow,
            child: Padding(
              padding: const EdgeInsets.all(3),
              child: Text("Ini paddind"),))
        ],) 
      ));
  }

  Widget _container(){
    return Container( 
        height: 200,
        width: 200,
        decoration: BoxDecoration(
          color: Colors.yellow,
          shape: BoxShape.circle,
          boxShadow: const [
            BoxShadow(blurRadius: 10)
          ],
          gradient: LinearGradient(colors: const [
            Colors.red,
            Colors.yellow
          ])
          //borderRadius: BorderRadius.all(Radius.elliptical(2,10)),
          //border: Border.all(width: 2, color: Colors.black)
          ),
          child: Center(
            child: Icon(Ionicons.ios_home, size: 100,)
            ) ,
      ) ;
  }
}