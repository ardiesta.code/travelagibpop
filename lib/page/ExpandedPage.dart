import 'package:flutter/material.dart';

class ExpandedPage extends StatefulWidget {
  ExpandedPage({Key key}) : super(key: key);

  @override
  _ExpandedPageState createState() => _ExpandedPageState();
}

class _ExpandedPageState extends State<ExpandedPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Expanded"),),
        body: Row(
          children: <Widget>[
            Expanded(child: Container(
              decoration: const BoxDecoration(
              color: Colors.red
            ),),
            flex: 2,
            ),
            Expanded(child: Container(
              decoration: const BoxDecoration(
              color: Colors.yellow
            ),),
            flex: 3,
            ),
            Expanded(child: Container(
              decoration: const BoxDecoration(
              color: Colors.blue
            ),),),
          ],
          ),
    );
  }
}