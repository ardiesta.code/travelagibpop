import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

class StackPage extends StatefulWidget {
  StackPage({Key key}) : super(key: key);

  @override
  _StackPageState createState() => _StackPageState();
}

class _StackPageState extends State<StackPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
       appBar: AppBar(title: Text("Stack"),),
       body: Stack(
         children: <Widget>[
           Positioned(
             top: 10,
             left: 10,
             child: Icon(Ionicons.ios_bicycle,size: 60,),
             ),
             Positioned(
             top: 10,
             right: 10,
             child: Icon(Ionicons.ios_cellular,size: 60,),
             ),
             Positioned( 
               bottom: 10, 
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                  Icon(Ionicons.ios_cafe, size: 60,),
                  Icon(Ionicons.ios_cafe, size: 60,),
                  Icon(Ionicons.ios_cafe, size: 60,),
                  Icon(Ionicons.ios_cafe, size: 60,)
                ],)
             ,
             ),
         ],
       ), 
    );
  }
}