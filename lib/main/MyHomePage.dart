import 'package:flutter/material.dart';
import 'package:travelagi/page/ContainerPage.dart';
import 'package:travelagi/page/ExpandedPage.dart';
import 'package:travelagi/page/PositionedPage.dart';
import 'package:travelagi/page/StackPage.dart';

class MyHomaPage extends StatefulWidget {
  MyHomaPage({Key key}) : super(key: key);

  @override
  _MyHomaPageState createState() => _MyHomaPageState();
}

class _MyHomaPageState extends State<MyHomaPage> {
  int iCountClick = 0;

  btnClickEvt(){
    setState(() {
       iCountClick +=1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(), 
      body: Center(child: IntrinsicWidth( 
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch, 
          children: <Widget>[
            RaisedButton(
              onPressed: (){
                Navigator.push(context, 
                  MaterialPageRoute(builder: (context) => StackPage())
                );
              },
              child: Text('Stack'),
              ),
              RaisedButton(
              onPressed: (){
                Navigator.push(context, 
                  MaterialPageRoute(builder: (context) => ExpandedPage())
                );
              },
              child: Text('Expanded'),
              ), 
              RaisedButton(
              onPressed: (){
                Navigator.push(context, 
                  MaterialPageRoute(builder: (context) => PositionedPage())
                );
              },
              child: Text('Positioned'),
              ),RaisedButton(
              onPressed: (){
                Navigator.push(context, 
                  MaterialPageRoute(builder: (context) => ContainerPage())
                );
              },
              child: Text('Container'),
              )
          ],
        ) ,) 
        ) , 
      );
  }

  Widget _appBar(){
    return AppBar(
        title: Text("Travelagi"),
        leading: Icon(Icons.menu),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.motorcycle),
            onPressed: (){
              print('Klik me');
              btnClickEvt();
            },
          ),
          IconButton(
            icon: const Icon(Icons.more_vert),
            onPressed: (){},
          ),
        ],
        );
  }
}
